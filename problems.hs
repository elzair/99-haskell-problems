import Data.Maybe
import qualified System.Random as R
import Control.Monad
import Data.List

myLast :: (Eq a) => [a] -> a
myLast (x:xs)
  | xs == []  = x
  | otherwise = myLast xs

myButLast :: (Eq a) => [a] -> a
myButLast (x:y:ys)
  | ys == []  = y
  | otherwise = myButLast (y:ys)

elementAt :: (Eq a, Eq b, Num b) => [a] -> b -> a
elementAt (x:xs) 1 = x
elementAt (x:xs) n = elementAt xs (n - 1)

-- myLengthHelper :: (Eq a) => [a] -> Integer -> Integer
-- myLengthHelper [] n = n
-- myLengthHelper (x:xs) n = myLengthHelper xs (n + 1)
-- 
-- myLength :: (Eq a) => [a] -> Integer
-- myLength [] = 0
-- myLength xs = myLengthHelper xs 0

myLength :: (Eq a) => [a] -> Integer
myLength [] = 0
myLength xs = foldl (\acc x -> acc + 1) 0 xs

myReverse :: (Eq a) => [a] -> [a]
myReverse xs = foldl (\acc x -> x : acc) [] xs

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == myReverse xs

data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (Elem a)      = [a]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List [])     = []

compress :: (Eq a) => [a] -> [a]
compress []     = []
compress (x:[]) = [x]
compress (x:y:ys)
  | x == y    = (compress (y:ys))
  | otherwise = [x] ++ (compress (y:ys))

pack :: (Eq a) => [a] -> [[a]]
pack = foldr func []
       where func x [] = [[x]]
             func x (y:xs) =
               if x == (head y)
               then ((x:y):xs)
               else ([x]:y:xs)

encode :: (Eq a) => [a] -> [(Integer, a)]
encode xs = map (\x -> ((myLength x), (head x))) (pack xs)

data EncodedValue a = Single a | Multiple Integer a deriving (Read, Show)

encodeModified :: (Eq a) => [a] -> [EncodedValue a]
encodeModified xs = map func (pack xs)
                    where func x =
                            if myLength x == 1
                            then Single (head x)
                            else Multiple (myLength x) (head x)

decode :: (Eq a) => [EncodedValue a] -> [a]
decode ((Single x):[]) = [x]
decode ((Single x):xs) = [x] ++ (decode xs)
decode ((Multiple n x):xs) = helper n x xs
  where helper n x [] = repeat n x
        helper n x xs = (repeat n x) ++ (decode xs)
        repeat 1 x    = [x]
        repeat n x    = [x] ++ (repeat (n - 1) x)

encodeDirect :: (Eq a) => [a] -> [EncodedValue a]
encodeDirect xs = helper [] Nothing xs
  where helper acc Nothing []                     = acc
        helper acc (Just cur) []                  = acc ++ [cur]
        helper acc Nothing (x:xs)                 = helper acc (Just (Single x)) xs
        helper acc (Just (Single cur)) (x:xs)     =
          if x == cur
          then helper acc (Just (Multiple 2 cur)) xs
          else helper (acc ++ [(Single cur)]) (Just (Single x)) xs
        helper acc (Just (Multiple n cur)) (x:xs) =
          if x == cur
          then helper acc (Just (Multiple (n + 1) cur)) xs
          else helper (acc ++ [(Multiple n cur)]) (Just (Single x)) xs

dupli :: (Eq a) => [a] -> [a]
dupli [] = []
dupli (x:xs) = [x,x] ++ (dupli xs)

repli :: (Eq a) => Integer -> [a] -> [a]
repli 0 _  = []
repli _ [] = []
repli n (x:xs) = [x] ++ (repli (n - 1) [x]) ++ (repli n xs)

dropEvery :: (Eq a) => [a] -> Integer -> [a]
dropEvery xs n = helper xs n n
  where helper [] _ _ = []
        helper (x:xs) cd ds
          | cd < 1    = []
          | cd == 1   = helper xs ds ds
          | otherwise = [x] ++ (helper xs (cd - 1) ds)

split :: (Eq a) => [a] -> Integer -> ([a], [a])
split xs n = helper xs [] [] n
  where helper [] ys zs _ = (ys, zs)
        helper (x:xs) ys zs n
          | n > 0 = helper xs (ys ++ [x]) zs (n - 1)
          | otherwise = helper xs ys (zs ++ [x]) 0

slice :: (Eq a) => [a] -> Integer -> Integer -> [a]
slice xs st en = helper xs st en 1 []
  where helper [] _ _ _ ret = ret
        helper (x:xs) st en ct ret
          | ct > en   = ret
          | ct >= st  = helper xs st en (ct + 1) (ret ++ [x])
          | otherwise = helper xs st en (ct + 1) ret

rotate :: (Eq a) => [a] -> Integer -> [a]
rotate xs n = func (split xs (num xs n))
  where func xs  = (snd xs) ++ (fst xs)
        num xs n = mod ((myLength xs) + n) (myLength xs)

removeAt :: (Eq a) => Integer -> [a] -> (a, [a])
removeAt n xs = func (split xs (n - 1))
  where func xt     = (head (fst (splitSnd xt)), (fst xt) ++ (snd (splitSnd xt)))
        splitSnd xt = split (snd xt) 1

insertAt :: (Eq a) => a -> [a] -> Integer -> [a]
insertAt x xs n = func x (split xs (n - 1))
  where func x xt = (fst xt) ++ [x] ++ (snd xt)

range :: Integer -> Integer -> [Integer]
range a b = helper a b []
  where helper a b st
          | a > b = st
          | otherwise = helper (a + 1) b (st ++ [a]) 

rndSelect :: (Eq a) => [a] -> Int -> IO [a]
rndSelect xs n = do
  gen <- R.getStdGen
  return $ take n [ xs !! x | x <- R.randomRs (0, (length xs) - 1) gen]

diffSelect :: Int -> Int -> IO [Int]
diffSelect n max = rndSelect [1..max] n

randPerm :: [a] -> IO [a]
randPerm xs = pick $ permutations xs
  where pick xs = do
          n <- R.randomRIO (0, (length xs - 1))
          return (xs !! n)
  
combinations :: Int -> [a] -> [[a]]
combinations n xs = filter (\x -> length x == n) (powerset xs)
  where powerset xs = filterM (\x -> [True, False]) xs

combination :: Int -> [a] -> [([a], [a])]
combination 0 xs = [([],xs)]
combination n [] = []
combination n (x:xs) = ts ++ ds
  where ts = [(x:ys,zs) | (ys,zs) <- combination (n-1) xs]
        ds = [(ys,x:zs) | (ys,zs) <- combination n xs]
